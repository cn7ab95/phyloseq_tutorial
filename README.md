# phyloseq_tutorial

[![Tested on R](https://img.shields.io/badge/Tested%20on%20R-version%204.2.3-red.svg)]()
[![Author](https://img.shields.io/badge/Author-Cyril%20Noël-yellow.svg)](https://github.com/cnoel-sebimer)

## Presentation

This repository is used to share data and script for the phyloseq tutorial developped by the SeBiMER.

## Prerequisites

- Install [R](https://pbil.univ-lyon1.fr/CRAN/)

- Install [RStudio](https://posit.co/download/rstudio-desktop/#download)

- Download/Clone the repository: 

```git clone https://gitlab.ifremer.fr/cn7ab95/phyloseq_tutorial.git```

-- (c) 2023 - Cyril Noël, SeBiMER, Ifremer
